from django import template

register = template.Library()


@register.filter(name="get_num_categories")
def get_num_receipts(object, all_receipts):
    num_receipts = 0
    for receipts in all_receipts:
        if receipts.category == object or receipts.account == object:
            num_receipts += 1
    return num_receipts
