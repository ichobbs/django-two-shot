from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


# Create your views here.
@login_required
def receipt_view(request):
    all_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "all_receipt_objects": all_receipts,
    }
    return render(request, 'receipts/home.html', context)


@login_required
def create_receipt(request):
    if request.method != "POST":
        form = ReceiptForm
    else:
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    all_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "category_objects": categories,
        "all_receipts_objects": all_receipts
    }
    return render(request, 'receipts/category_list.html', context)


@login_required
def account_list(request):
    categories = Account.objects.filter(owner=request.user)
    all_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "account_objects": categories,
        "all_receipts_objects": all_receipts
    }
    return render(request, 'receipts/account_list.html', context)


@login_required
def create_category(request):
    if request.method != "POST":
        form = CategoryForm
    else:
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")

    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method != "POST":
        form = AccountForm
    else:
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")

    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
